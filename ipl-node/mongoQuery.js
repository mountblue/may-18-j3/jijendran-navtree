let mongoose = require('mongoose');
let bodyParser = require('body-parser');
let path = require('path');
let matches = require('./models/matches');
let delivery = require('./models/delivery');

//make a connection..
mongoose.connect('mongodb://localhost:27017/iplDataSet');
let dbStatus = mongoose.connection;
dbStatus.on('error', console.error.bind(console, "connection error"));
dbStatus.once('open', function () {
  console.log("Connection to testDb is open...");
});
numberOfYears = (result) => {
  matches.aggregate([{
    $group: {
      _id: '$season'
    }
  },
  {
    $sort: { _id: 1 }
  }
  ], function (err, data) {
    if(err)
    {
      result(err);
    }
    else {
      result(data);
    }
  });
}
teamName = (res,year) => {
  matches.aggregate([{
    $match: { 'season': Number(year) }
  },
  {
    $group :{_id:'$team1'}
  }
  ], function (err, data) {
    if (err)
      res.send(err);
    else
      res.send(data);
  });
}


playerSet = (res,year,team) => {
  console.log(team);
  matches.aggregate([
  {
    $match: { 'season': Number(year) }
  },
  {
    $lookup: {
      from: 'deliveryData',
      localField: 'id',
      foreignField: 'match_id',
      as: 'playersInfo'
    }
  },
  {
    $unwind: '$playersInfo'
  },
{
  $group:{
    _id:{
      teams:'$playersInfo.batting_team',
      player:'$playersInfo.batsman'
    }
  }
},
{
  $project:{
    _id:0,
    name:'$_id.teams',
    play:'$_id.player'
  }
},
{
  $match:{
    name:team
  }
},
{
  $project:{
      batsman:'$play'
  }
}
], function (err, data) {
      if (err)
        res.send(err);
      else
        res.send(data);
    });
}
playerDet = (res,year,team,player) =>
{
  matches.aggregate([
  {
    $match: { 'season': Number(year) }
  },
  {
    $lookup: {
      from: 'deliveryData',
      localField: 'id',
      foreignField: 'match_id',
      as: 'playersInfo'
    }
  },
  {
    $unwind: '$playersInfo'
  },
{
  $group:{
    _id:{
      teams:'$playersInfo.batting_team',
      player:'$playersInfo.batsman'
    },
    count :{ $sum :'$playersInfo.batsman_runs'}
  }
},
{
  $project:{
    _id:0,
    name:'$_id.teams',
    play:'$_id.player',
    runs:'$count'
  }
},
{
  $match:{
    name:team,
    play:player
  }
},
{
  $project:{
      run:'$runs'
  }
}

], function (err, data) {
      if (err)
        res.send(err);
      else
        res.send(data);
    });
}
module.exports = { numberOfYears, teamName, playerSet, playerDet }
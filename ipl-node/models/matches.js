let mongoose=require('mongoose');
//define the schema for matches collection
let iplMatchSchema = mongoose.Schema(
{
	id:Number,
  	season:Number,
  	city: String,
  	date: String,
  	team1: String,
  	team2: String,
  	toss_winner: String,
  	toss_decision: String,
  	result: String,
  	dl_applied:Number,
  	winner: String,
  	win_by_runs:Number,
  	win_by_wickets:Number,
  	player_of_match: String,
  	venue: String,
  	umpire1: String,
  	umpire2: String,
  	umpire3: String

});

module.exports = mongoose.model('iplMatch',iplMatchSchema,'matchData');
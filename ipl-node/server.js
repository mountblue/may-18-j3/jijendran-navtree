const express = require('express');
let mongoose = require('mongoose');
let bodyParser = require('body-parser');
let path = require('path');
let app = express();
let Cors = require('cors');
const port = 8084;
let iplCalculation = require('./mongoQuery.js');
let matches = require('./models/matches');
//homepage folder path declaration
app.use(express.static(path.join(__dirname, '/public')));
app.use(Cors());
//counting the years
app.get('/years', function (req, res) {
	iplCalculation.numberOfYears(function(data) {
		if(data)
			res.send(data);
		else
			res.sendStatus(500).json({ msg: "error in server" });
	});
})
//getting the teams for particular year..
app.get('/teamName/:id', function (req, res) {
	let year=req.params.id;
	iplCalculation.teamName(res,year);
})
//getting from db(winning count)
app.get('/playerSet/:year/:team', function (req, res) {
	let year=req.params.year;
	let team=req.params.team;
	console.log(year);
	iplCalculation.playerSet(res,year,team);
	
})
app.get('/playerDet/:year/:team/:player',function(req,res)
{
	let year=req.params.year;
	let team=req.params.team;
	let player=req.params.player;
	iplCalculation.playerDet(res,year,team,player);
})
app.listen(port, function () {
	console.log("running server on " + port);
})
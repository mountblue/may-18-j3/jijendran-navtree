import React, {Component} from 'react';
import PlayerDetails from './playerDetails';
class PlayersData extends Component
{
	constructor()
	{
		super();
		this.state={
			hideCond:false,
			runs:[]
		}
	}
	updateHides()
	{
		let check =this.state.hideCond;
		if(!check)
		{
			this.setState({
				hideCond:true
			});
			this.playerDetail();
		}
		else
			this.setState({
				hideCond:false,
				runs:[]
			});
		
	}
	playerDetail()
	{
		fetch('http://localhost:8084/playerDet/'+this.props.year+'/'+this.props.team+'/'+this.props.player).then(function(req){return req.json()})
		.then(data =>
		this.setState({runs:data})
		);
	}
	render()
	{
		let player=this.props.player;
		return ([<li className="playerList" onClick={this.updateHides.bind(this)}>{"player-->"+player}</li>,
			<ul>
			{this.state.runs.map((run,i) => {return <PlayerDetails key={i} runs={run.run} />})}
			</ul>]);
		
	}
}
export default PlayersData;
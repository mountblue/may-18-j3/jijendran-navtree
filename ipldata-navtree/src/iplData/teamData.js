import React, {Component} from 'react';
import PlayerData from './playersData';
class TeamData extends Component
{
	constructor()
	{
		super();
		this.state={
			hideCond:false,
			players:[]
		}
	}
	updateHide(){
		let check =this.state.hideCond;
		if(!check)
		{
			this.setState({
				hideCond:true
			});
			this.playerData();
		}
		else
			this.setState({
				hideCond:false,
				players:[]
			});
		
	}
	playerData()
	{
		fetch('http://localhost:8084/playerSet/'+this.props.year+'/'+this.props.team).then(function(req){return req.json()})
		.then(data =>
		this.setState({players:data})
		);
	}
	render()
	{
		let team=this.props.team;
		let Year=this.props.year;
		return ([
			<li className ="teamlist" onClick={this.updateHide.bind(this)}>{"IPL team-->"+team}</li>,
			<ul>
			{this.state.players.map((player,i) => {return <PlayerData key={i} year={Year} team={team} player={player.batsman} />})}
			</ul>
			]);
	}
}
export default TeamData;
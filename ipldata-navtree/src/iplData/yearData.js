import React, { Component } from 'react';
import TeamData from './teamData';
class YearData extends Component {
	constructor(props) {
		super(props);
		this.state = {
			//hideCond:[],
			teams: [],
			selectedYear: undefined

		}
	}
	update(key, event) {
		console.log("id::", event.target.id, "selectedYear::", this.state.selectedYear)
		/*let conditionArray=[];
		if(!this.state.hideCond[key])
		{
			conditionArray = this.state.hideCond.fill(false);
			conditionArray[key]= true;
			console.log(conditionArray);
		}
		else
		{
			conditionArray=this.state.hideCond.fill(false);
			conditionArray[key]= false;
			console.log(conditionArray);
		}*/
		if (event.target.id === this.state.selectedYear) {
			this.setState({
				teams:[],
				selectedYear: undefined
			})
			console.log("dfdfdfd");
		}
		else {
			this.setState({
				selectedYear: event.target.id,
			})
			this.teamDetails();
		}
	}
	//this.setState({hideCond:conditionArray});


	teamDetails() {
		fetch('http://localhost:8084/teamName/' + this.props.year).then(function (req) { return req.json() })
			.then(data =>
				this.setState({ teams: data })
			);
	}
	render() {
		let Year = this.props.year;
		return ([<li id={Year} className="list-style" onClick={this.update.bind(this,this.props.id)}>{Year}</li>,
			<ul> 
				{this.state.teams.map((team, i) => {
					if(this.state.selectedYear == Year){
						return (<TeamData key={i} year={Year} team={team._id} />);
					}
				})
			}
			</ul>

		])
	}
}

export default YearData;



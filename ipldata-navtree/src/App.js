import React, { Component } from 'react';
import './App.css';
import image from './image1.jpeg';
import YearData from './iplData/yearData';
class App extends Component {
constructor(){
	super();
	this.state={
		years:[],
		hideCond:false
		
		}
	}
	update(event){
		let check =this.state.hideCond;
		if(!check)
		{
			this.setState({
				hideCond:true
			});
			this.getYearsData();
		}	
		else
			this.setState({
				hideCond:false,
				years:[]
			});
	}
getYearsData()
{
	fetch('http://localhost:8084/years').then(function(req){ return req.json() })
	.then(data =>
		this.setState({years:data})
	)
}
render() {
    return (
    	[<div className="head" onClick={this.update.bind(this)}><img className="img-style" src={image} /><h1 className="head-style">IPL STATUS</h1></div>,
    	<ul className="cont-style">
    	{
      	this.state.years.map((year,i) => { return <YearData id={i} year={year._id}/> })
      	}</ul>]
    );
  }
}

export default App;
